@if(session()->has('flash_message'))
	<input type="hidden" id="level" value="{{ session('flash_message.level') }}">
	<input type="hidden" id="title" value="{{ session('flash_message.title') }}">
	<input type="hidden" id="message" value="{{ session('flash_message.message') }}">
	<script type="text/javascript">
		swal({
		  	position: 'bottom-left',
		  	width: 350,
		  	type: document.getElementById("level").value,
		  	title: document.getElementById("title").value,
		  	text: document.getElementById("message").value,
		  	showConfirmButton: false,
		  	timer: 5000
		})
	</script>
@endif
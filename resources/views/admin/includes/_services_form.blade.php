<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('value', (old('value')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'data-validate-length-range'	=> '6',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('value'))
            <span class="help-block">
                <strong>{{ $errors->first('value') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Price <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('price', (old('price')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required',
			]) !!}
			@if ($errors->has('price'))
            <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>
</div>
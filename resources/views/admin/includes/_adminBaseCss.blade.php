    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/nprogress/nprogress.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style-admin.css') }}" />
<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('admin.home') }}" class="site_title"> <span>Meadows</span></a>
        </div>
        <div class="clearfix"></div>
        <div class="profile clearfix">
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
            </div>
        </div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-users"></i> Details <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('admin.user.list') }}">Users</a></li>
                            <li><a href="{{ route('admin.order.incomplete') }}">Incomplete Orders</a></li>
                            <li><a href="{{ route('admin.order.list') }}">Orders</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.services.list') }}"><i class="fa fa-cogs"></i> Services </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.fuel.list') }}"><i class="fa fa-cogs"></i> Fuels </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.zip.code.list') }}"><i class="fa fa-cogs"></i> Zip Code </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.tank.capacity.list') }}"><i class="fa fa-cogs"></i> Tank Capacity </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.tank.how_much.list') }}"><i class="fa fa-cogs"></i> How Much Is In Tank </a>
                    </li>
                </div>
            </div>
        </div>
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="javascript:void(0)">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Capacity <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('capacity', (old('capacity')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'data-validate-length-range'	=> '6',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('capacity'))
            <span class="help-block">
                <strong>{{ $errors->first('capacity') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">State Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('name', old('name'), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Zip Code <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('code', (old('code')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('code'))
            <span class="help-block">
                <strong>{{ $errors->first('code') }}</strong>
            </span>
        @endif
    </div>
</div>
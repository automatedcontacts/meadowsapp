<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">How Much Is In Tank <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('how_much_tank', (old('how_much_tank')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'data-validate-length-range'	=> '30',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('how_much_tank'))
            <span class="help-block">
                <strong>{{ $errors->first('how_much_tank') }}</strong>
            </span>
        @endif
    </div>
</div>

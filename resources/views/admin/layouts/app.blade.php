<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Admin || Meadows</title>
	<!-- CSS -->
	@include ('admin.includes._adminBaseCss')

	@yield ('custom-css')
</head>
<body class="nav-md">
	<div class="container body">
      	<div class="main_container">
      		@include ('admin.includes._sideBar')
      		@include ('admin.includes._topNav')
      		<div class="right_col" role="main">
				@yield ('content')
			</div>
			@include ('admin.includes._footer')
		</div>
	</div>
	@include ('admin.includes._adminBaseJs')
	@yield ('custom-js')
</body>
</html>
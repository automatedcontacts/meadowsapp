@extends ('admin.layouts.app')

@section ('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Update Service</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                  			<h2>Update <small>( {{ $data->how_much_tank }} )</small></h2>
                  			<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
			                {!! Form::model($data, ['route' => 'admin.how_much.update', 'class' => 'form-horizontal form-label-left', 'novalidate']) !!}
			                	@include ('admin.includes._how_much_tank_form')
			                    <div class="item form-group">
			                    	<div class="col-md-6 col-md-offset-3">
			                    	 	{!! Form::hidden('id', $data->id) !!}
			                    		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
			                    	</div>	
			                    </div>
			                {!! Form::close() !!}
			            </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section ('custom-js')
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/validator/validator.js') }}"></script>
@endsection
@extends ('admin.layouts.app')

@section ('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Manage How Much Is In Tank</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                  			<h2>Add More <small>Is In Tank List</small></h2>
                  			<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
			                {!! Form::open(['route' => 'admin.how_much.add', 'class' => 'form-horizontal form-label-left', 'novalidate']) !!}
			                	@include ('admin.includes._how_much_tank_form')
			                    <div class="item form-group">
			                    	<div class="col-md-6 col-md-offset-3">
			                    		{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
			                    	</div>	
			                    </div>
			                {!! Form::close() !!}
			            </div>
                  	</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                  			<h2>How Much Is In Tank</h2>
                  			<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
			                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			                    <thead>
			                        <tr>
			                        	<th>SNo</th>
			                        	<th>How Much Is In Tank</th>
			                        	<th>Action</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        @php $i=1; @endphp
			                    	@foreach ($how_much_tanks as $how_much_tank)
				                        <tr>
					                        <td>{{ $i }}</td>
					                        <td>{{ $how_much_tank->how_much_tank }}</td>
					                        <td>
					                        	<a href="{{ route('admin.how_much.edit', $how_much_tank->id) }}">
					                        		<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					                        	</a>
					                        </td>
				                        </tr>
				                       @php $i++; @endphp
				                    @endforeach
			                    </tbody>
			                </table>
			            </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>    
@endsection

@section ('custom-js')
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/validator/validator.js') }}"></script>
@endsection

@extends ('admin.layouts.app')

@section ('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>User Data</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                  			<h2>Update</h2>
                  			<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
			                {!! Form::model($data, ['route' => 'admin.user.update', 'class' => 'form-horizontal form-label-left', 'novalidate']) !!}
			                	@include ('admin.includes._user_form', ['selected_zip_code' => $zip_code->id, 'emailProp' => true])
			                    <div class="item form-group">
			                    	<div class="col-md-6 col-md-offset-3">
			                    	 	{!! Form::hidden('id', $data->id) !!}
			                    		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
			                    	</div>	
			                    </div>
			                {!! Form::close() !!}
			            </div>
                  	</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Details</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if (!count($data->profile))
                                <center><h3>No Details Yet</h3></center>
                            @endif
                            @foreach ($data->profile as $profile)
                                <div class="col-md-4">
                                    <p>{{ $profile->title }} {{ $profile->first_name }} {{ $profile->last_name }}</p>
                                    <p>{{ $profile->phone }}, {{ $data->email }}</p>
                                    <p>Contact On: {{ $profile->contact_on }}</p>
                                    <p>Referred By: {{ $profile->ref_by }}</p>
                                    <p>{{ $profile->street }}, {{ $profile->city }}, {{ $data->zip_code }}</p>
                                    <br>
                                    <br>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection

@section ('custom-js')
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/validator/validator.js') }}"></script>
@endsection
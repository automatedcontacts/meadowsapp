<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                
                margin: 0;
            }
            .landing-body:before{ content: ""; background-color: rgba(0,0,0,.8); position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .landing-btn {
               float: left;
               width: 100%;
               margin-top: 50px;
            }
             .landing-btn a{color: #fff;
                padding: 11px 35px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase; position: relative; border:solid 2px #719430; display: inline-block; vertical-align: top; border-radius: 30px; min-width:250px ;transition: all 1s ease;
    -o-transition: all 1s ease;
    -webkit-transition: all 1s ease;
    transition: all 1s ease;}
.landing-btn a:hover{ background:#719430;}

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase; position: relative;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .links > a:after{content: ""; position:absolute; background:#719430; width: 7px;  height: 7px; right: 0px; top:0px; bottom: 0px; border-radius: 50%; margin:auto;  margin: auto;-moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -webkit-transition: all 1s ease;
    transition: all 1s ease;}
            .links > a:hover{ color: #719430}
            .links > a:last-child:after{ display: none; }            
        </style>
    </head>
    <body class="landing-body" style="background-image:url({{ asset('img/meadows-banner.jpg') }}); background-position:center center; background-repeat:no-repeat; background-size:cover">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <img src="{{ asset('img/meadowspetroleumlogo.png') }}" alt="logo">
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
@if (Route::has('login'))
                <div class="landing-btn">
                    @auth
                        <a href="{{ route('fuel.details.show') }}">Start</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif


            </div>
        </div>
    </body>
</html>

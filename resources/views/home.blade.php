@extends('layouts.app')

@section('content')
    <div class="panel-heading">Dashboard</div>
    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <center>
        	<a href="{{ route($link) }}" class="btn btn-primary" id="sendToStart" >{{ $text }}</a>
        </center>
    </div>
@endsection

<!DOCTYPE html>
<html>
<head>
	<title>EMAIL Templete</title>
	@include ('emails.includes._style')
</head>
<body>
	@yield('content')
</body>
</html>
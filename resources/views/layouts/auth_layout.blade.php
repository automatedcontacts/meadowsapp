<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Meadows') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
    <style type="text/css">
        .swal2-icon.swal2-info {
            float: left !important;
        }
        .swal2-icon.swal2-success {
            float: left !important;
        }
        .swal2-popup.swal2-modal.swal2-show {
            display: block !important;
            max-height: 150px !important;
        }
        .swal2-popup .swal2-title {
            margin-top: 15px !important;
        }
    </style>
</head>
<body>
    <div id="app">
        @include ('includes._nav_bar')

        @yield('content')
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.js"></script>
    @include ('includes._flash')
    @include ('includes._javascript')
</body>
</html>
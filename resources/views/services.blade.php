@extends('layouts.app')

@section('content')
    <div class="panel-heading">Additional Services</div>
    <div class="panel-body row">
        <div class="col-md-12">
            <h4><center>Please Select The Additional Service (s) You Want.</center></h4>
        </div>
        {!! Form::open(['route' => 'services.save', 'class' => 'form-horizontal']) !!}
            @foreach ($services as $service)  
                <div class="services-col">
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <?php 
                                $checked = false;
                                if (in_array($service->id, $selected)) {
                                    $checked = true;
                                }
                            ?>
                            {!! Form::checkbox('service[]', $service->id, $checked) !!}
                            {{ $service->value }} ( $ {{ $service->price }} )
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <a href="{{ route('user.data.view') }}" class="btn btn-primary pull-left">Previous</a>
                    {!! Form::submit('Next', ['class' => 'btn btn-primary pull-right']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zip_code_id')->unsigned()->nullable();
            $table->foreign('zip_code_id')->references('id')->on('zip_codes')->onDelete('cascade');
            $table->string('rate_per_gallon');
            $table->integer('is_global')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_rates');
    }
}

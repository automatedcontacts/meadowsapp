<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('fuel_detail_id')->unsigned()->nullable();
            $table->foreign('fuel_detail_id')->references('id')->on('fuel_details')->onDelete('cascade');
            $table->integer('tank_detail_id')->unsigned()->nullable();
            $table->foreign('tank_detail_id')->references('id')->on('tank_details')->onDelete('cascade');
            $table->integer('instruction_id')->unsigned()->nullable();
            $table->foreign('instruction_id')->references('id')->on('instructions')->onDelete('cascade');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->integer('fuel_rate_id')->unsigned()->nullable();
            $table->foreign('fuel_rate_id')->references('id')->on('fuel_rates')->onDelete('cascade');
            $table->string('amount')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

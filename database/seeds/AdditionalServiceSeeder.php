<?php

use App\AdditionalService;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class AdditionalServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=0; $i < 2; $i++) { 
        	AdditionalService::create([
        		'value'		=> $faker->sentence($nbWords = 6, $variableNbWords = true),
        		'price'		=> array_rand(['2.333' => '2.333', '9.012' => '9.012', '1.011' => '1.011'])
        	]);
        }
    }
}

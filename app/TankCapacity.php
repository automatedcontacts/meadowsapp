<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TankCapacity extends Model
{
     protected $table = 'tank_capacities';
     protected $fillable = ['capacity'];
}

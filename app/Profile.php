<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'title', 'first_name', 'last_name', 'street', 'city', 'phone', 'color_home', 'contact_on', 'ref_by','is_existing',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FuelDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'user_id', 'gallons', 'amount', 'need_express', 'prime_start','fill_tank'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

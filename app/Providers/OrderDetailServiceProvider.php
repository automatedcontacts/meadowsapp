<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrderDetailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('includes._order_detail', 'App\Helpers\ViewHelper@getDataForOrderDetails');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

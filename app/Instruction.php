<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instruction extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'user_id', 'instruction', 'auto_delivery',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

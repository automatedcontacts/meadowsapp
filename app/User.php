<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'zip_code', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fuelDetails()
    {
        return $this->hasMany('App\FuelDetail');
    }

    public function tankDetails()
    {
        return $this->hasMany('App\TankDetail');
    }

    public function instruction()
    {
        return $this->hasMany('App\Instruction');
    }

    public function profile()
    {
        return $this->hasMany('App\Profile');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalService extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'value', 'price'
    ];

    public function order()
    {
    	return $this->belongsToMany('App\Order');
    }
}

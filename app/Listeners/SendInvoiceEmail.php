<?php

namespace App\Listeners;

use Mail;
use App\Events\PaymentWasDone;
use App\Mail\{ SendInvoiceToUser, SendInvoiceToAdmin};
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvoiceEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentWasDone  $event
     * @return void
     */
    public function handle(PaymentWasDone $event)
    {
        $user = $event->order->user()->first();
        
        $event->order->update([
            'status'    => 1
        ]);

        $profile = $event->order->profile()->first();
        $fuel_detail = $event->order->fuelDetail()->first();
        $add_ons = $event->order->addOn()->get();
        $payment = $event->payment;
        $card_number = $event->payment->card_digit;
        $user = $event->order->user()->first();

        Mail::to($user->email)->send(new SendInvoiceToUser($event->order, $profile, $add_ons, $fuel_detail, $payment, $user,$card_number));
        Mail::to(env('ADMIN_EMAIL_ID', 'shubham.sinha@iskpro.com'))->send(new SendInvoiceToAdmin($event->order, $profile, $add_ons, $fuel_detail, $payment, $user,$card_number));
    }
}

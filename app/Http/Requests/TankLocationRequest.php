<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TankLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tank_location'     => 'required|integer',
            'tank_capacity'     => 'required',
            'current_status'    => 'required',
        ];
    }
}

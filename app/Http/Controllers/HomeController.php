<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->only('showHomePage');
        $this->middleware('auth')->only('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $link = 'fuel.details.show';
        $text = 'Start Here';

        $steps = $this->getAllStepData($request);
        // return $steps['step_2'];
        if (empty($steps['step_5'])) {
            $link = 'user.checkout.view';
            $text = 'Complete you payment';
        }
        
        if (empty($steps['step_4'])) {
            $link = 'user.data.view';
            $text = 'Complete you data';
        }

        if (empty($steps['step_3'])) {
            $link = 'delivery.instruction.view';
            $text = 'Complete delivery instruction';
        }

        if (empty($steps['step_2'])) {
            $link = 'tank.location.view';
            $text = 'Fill Tank Location';
        }

        return view('home', compact('link', 'text'));
    }

    public function showHomePage()
    {
        return view('auth.login');
    }

    public function emailToUser()
    {
        try {
            return view('emails.invoice_to_user');
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function emailToAdmin()
    {
        try {
            return view('emails.invoice_to_admin');
        } catch (\Exception $e) {
            dd($e);
        }
    }
}

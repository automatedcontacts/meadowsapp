<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashBoardController extends Controller
{

   	public function showDashBoard()
   	{
   		try {
   			return view('admin.dashboard');
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}


}

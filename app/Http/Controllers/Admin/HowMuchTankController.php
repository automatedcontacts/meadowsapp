<?php

namespace App\Http\Controllers\Admin;

use App\{ HowMuchTank};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HowMuchTankController extends Controller
{
    public function index(){
       $how_much_tanks=HowMuchTank::all();
       return view('admin.how_much_tank.list_how_much_tank',compact('how_much_tanks'));
	}
	public function create(Request $request){
		try {
   			HowMuchTank::create([
   				'how_much_tank'		=> $request->how_much_tank,
   			]);
            return redirect()->back();;
   		} catch (\Exception $e) {
   			dd($e);
   		}
	}
	public function edit(int $id)
	{
		try {
			$data = HowMuchTank::where('id', $id)->first();
			if (empty($data)) {
				flash()->info('OOPS!', 'Try Again with valide ID');
				return redirect()->route('admin.services.list');
			}
         return view('admin.how_much_tank.edit', compact('data'));
		} catch (\Exception $e) {
			dd($e);
		}
	}
	public function update(Request $request){
		try {
			$data = HowMuchTank::where('id', $request->id)->first();

			if (empty($data)) {
				flash()->info('OOPS!', 'Try Again with valide ID');
				return redirect()->route('admin.tank.how_much.list');
			}

			$data->update([
				'how_much_tank'		=> $request->how_much_tank,
			]);

			return redirect()->route('admin.tank.how_much.list');
		} catch (\Exception $e) {
			dd($e);
		}
	}
}

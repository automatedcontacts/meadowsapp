<?php

namespace App\Http\Controllers;

use Auth;
use App\Order;
use Illuminate\Http\Request;
use App\Events\PaymentWasDone;

class PaymentController extends Controller
{
    public function __construct(Order $order)
    {
    	$this->middleware('auth');
    	$this->order = $order;
    }

    /**
     * Function to show the checkout page
     * 
     * @param  Request
     * @return [view] checkout
     */
    public function showCheckoutPage(Request $request)
    {
        try {
        	$is_old = false;
        	$data = $this->getAllStepData($request);

        	if ($data['step_1'] == null) {
        		$is_old = true;
        	}

        	$order = Auth::user()->order()->latest('created_at')->with('fuelDetail')->with('profile')->with('addOn')->first();

            return view('checkout', compact('order', 'is_old'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * Function For Charging Card Of User Based On Stripe Token
     * 
     * @param  [Stripe TOKEN] $token 
     * @param  [int] $order_id
     * @param  Request $request
     * @return [redirect] route('fuel.details.show')
     */
    public function chargeCard($token, $order_id, Request $request)
    {
		try {
			$order = $this->order->where('id', $order_id)->first();
            

			\Stripe\Stripe::setApiKey(env('STRIPE_API_KEY', ''));


		 	$customer = \Stripe\Customer::create([
		      	'email' => Auth::user()->email,
		      	'source'  => $token
		  	]);

            $customer_email=$customer->email;


		  	$charge = \Stripe\Charge::create([
		      	'customer' => $customer->id,
		      	'amount'   => $order->amount * 100,
		      	'currency' => 'usd'
		  	]);
            $card_last4=$charge->source->last4;

		  	$payment = Auth::user()->payment()->create([
				'customer_id_stripe'	=> $charge->customer,
				'id_stripe'				=> $charge->id,
				'card_fingerprint'		=> $charge->source->fingerprint,
				'status'				=> $charge->status,
				'name'					=> $charge->source->name,
				'order_id'				=> $order->id,
                'card_digit'            => $card_last4,
		  	]);
            //dd($payment);
		  	if ($charge->status == 'succeeded') {
                event(new PaymentWasDone($order, $payment));
		  		flash()->success('Success', 'Your Payment was Done successfully');		
		  	} else {
		  		flash()->info('OPSS', 'Payment is in Pending state');
		  	}

		  	$this->initSessionData($request);

		  	return redirect()->route('fuel.details.show');
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }
}
